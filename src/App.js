import React, { useState } from 'react';
import Firebase from './firebase';
import "firebase/auth";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import './App.css';

function App() {

  // register
  const [email, setEmail] = useState();
  const [pass, setPass] = useState();

  // login
  const [loginEmail, setLoginEmail] = useState();
  const [loginPass, setLoginPass] = useState();
  const [loginInfo, setLoginInfo] = useState('');

  const changeEmail = (e) => {
    setEmail(e.target.value);
  }
  const changePassword = (e) => {
    setPass(e.target.value);
  }

  const changeLoginEmail = (e) => {
    setLoginEmail(e.target.value);
  }
  const changeLoginPassword = (e) => {
    setLoginPass(e.target.value);
  }

  async function register(email, pass) {
    try {
      const createUser = await Firebase.auth().createUserWithEmailAndPassword(email, pass);
      console.log(createUser);
    } catch (error) {
      alert (error);
    }
  }

  async function login(email, pass) {
    try {
      console.log(email, pass);
      const loginUser = await Firebase.auth().signInWithEmailAndPassword(email, pass);
      setLoginInfo('berhasil login');
      getUserInfo();
      console.log(loginUser);
    } catch (error) {
      console.log(error);
    }
  }

  function getUserInfo() {
    Firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log(user);
      } else {
        return {};
      }
    });
  }

  return (
    <div className="App container">

      {/* Menu Register */}
      <h1>Daftar User</h1>
      <Form>
        <FormGroup>
          <Label for="exampleEmail">Email</Label>
          <Input 
            type="email" 
            name="email" 
            id="exampleEmail" 
            placeholder="with a placeholder" 
            value={email}
            onChange={changeEmail}
          />
          <Label for="examplePassword">Password</Label>
          <Input 
            type="password" 
            name="password" 
            id="examplePassword" 
            placeholder="password placeholder" 
            value={pass}
            onChange={changePassword}
          />
        </FormGroup>
        <Button 
          color="secondary" 
          size="lg" 
          block
          onClick={() => register(email,pass)}
        >Register</Button>
      </Form>

      {/* Menu Login */}
      <h1>Login User</h1>
      <Form>
        <FormGroup>
          <Label for="exampleEmail">Email</Label>
          <Input 
            type="email" 
            name="email" 
            id="exampleEmail" 
            placeholder="with a placeholder" 
            value={loginEmail}
            onChange={changeLoginEmail}
          />
          <Label for="examplePassword">Password</Label>
          <Input 
            type="password" 
            name="password" 
            id="examplePassword" 
            placeholder="password placeholder" 
            value={loginPass}
            onChange={changeLoginPassword}
          />
        </FormGroup>
        <Button 
          color="warning" 
          size="lg" 
          block
          onClick={() => login(loginEmail,loginPass)}
        >Login</Button>
      </Form>
      <h4>{loginInfo}</h4>
    </div>
  );
}

export default App;
