import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDGnfLNsra6WhLVXlNd__Oj814O1AH7QLU",
  authDomain: "binar-firebase.firebaseapp.com",
  databaseURL: "https://binar-firebase.firebaseio.com",
  projectId: "binar-firebase",
  storageBucket: "binar-firebase.appspot.com",
  messagingSenderId: "472423715844",
  appId: "1:472423715844:web:a5102412fe20c37e5f5151"
};

// Initialize Firebase
const Firebase = firebase.initializeApp(firebaseConfig);

export default Firebase;